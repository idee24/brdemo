package com.example.android.brdemo.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.android.brdemo.MainActivity;

public class Receiver extends BroadcastReceiver {

    private static final String TAG = Receiver.class.getSimpleName();
    private String message = "Message from BroadCast Receiver 1";

    @Override
    public void onReceive(Context context, Intent intent) {

            Log.i(TAG, message);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }
}
