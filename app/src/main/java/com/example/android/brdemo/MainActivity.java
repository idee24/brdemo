package com.example.android.brdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.brdemo.Receivers.MyResultReceiver;
import com.example.android.brdemo.Receivers.Receiver;

public class MainActivity extends AppCompatActivity{

    private Button sendBroadCastButton;
    private Receiver receiver;
    private TextView textView;
    private int counter = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);

        //sendBroadCastButton = findViewById(R.id.sendBroadCastButton);
        receiver = new Receiver();
    }

    public void sendBroadcastMessage(View view) {

        Intent broadcastIntent = new Intent("com.broadcast.receiver.action");

        Bundle broadcastBundle = new Bundle();
        broadcastBundle.putString("title", "BroadcastReceiver");

        sendOrderedBroadcast(broadcastIntent, null, new MyResultReceiver(),
                null, -1, "Android Broadcast", broadcastBundle);
    }


    public void unregisterReceiver(View view) {

        unregisterReceiver(timeTickReceiver) ;
    }

    public void registerReceiver(View view) {

        IntentFilter timeTickFilter = new IntentFilter();
        timeTickFilter.addAction(Intent.ACTION_TIME_TICK);
        registerReceiver(timeTickReceiver, timeTickFilter);
    }

    private BroadcastReceiver timeTickReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String message = "Broadcast from TimeTick Receiver";
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

            int minutes = counter;
            textView.setText(minutes + "Minute Over");
            counter++;

        }
    };

    public void lunchStickBroadcast(View view) {

        Intent stickyIntent = new Intent(MainActivity.this, StickyBroadcast.class);
        startActivity(stickyIntent);

    }

    public void lunchSecurity(View view) {

        Intent securityIntent = new Intent(MainActivity.this, Security.class);
        startActivity(securityIntent);
    }

    public void lunchLocalBroadcastActivity(View view) {

        Intent localBroadcastIntent = new Intent(MainActivity.this, LocalBroadcast.class);
        startActivity(localBroadcastIntent);
    }

    public void lunchPermissionActivity(View view) {

        Intent permissionIntent = new Intent(MainActivity.this, PermissionsActivity.class);
        startActivity(permissionIntent);
    }

    public static class InnerReceiver extends BroadcastReceiver{

        final String TAG = InnerReceiver.class.getSimpleName();
        String message = "Message from Inner BroadCast Receiver 2";

        @Override
        public void onReceive(Context context, Intent intent) {

                Log.i(TAG, message);
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(receiver);
    }
}
