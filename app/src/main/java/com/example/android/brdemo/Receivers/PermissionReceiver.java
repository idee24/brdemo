package com.example.android.brdemo.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class PermissionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String phoneState = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

        if (phoneState.equals(TelephonyManager.EXTRA_STATE_RINGING)){

            Toast.makeText(context, "Phone State: " + phoneState, Toast.LENGTH_SHORT).show();
        }

        if (phoneState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){

            Toast.makeText(context, "Phone State: " + phoneState, Toast.LENGTH_SHORT).show();
        }

        if (phoneState.equals(TelephonyManager.EXTRA_STATE_IDLE)){

            Toast.makeText(context, "Phone State: " + phoneState, Toast.LENGTH_SHORT).show();
        }
    }
}
