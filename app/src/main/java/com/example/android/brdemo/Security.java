package com.example.android.brdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Security extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);
    }



    public void sendBroadcast(View view) {

        Intent securityBroadcastIntent = new Intent("android.intent.action.AIRPLANE_MODE");
        sendBroadcast(securityBroadcastIntent, "brReceiver.permission.granted");
    }
    
}
