package com.example.android.brdemo;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.jar.Manifest;

public class PermissionsActivity extends AppCompatActivity {

    private static final String TAG = PermissionsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        int permissionCheck = ContextCompat.checkSelfPermission
                (this, android.Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions
                    (this, new String[]{android.Manifest.permission.READ_PHONE_STATE}, 8);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case 8:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                    String message = "Permission Granted to Read Phone State";
                    Log.i(TAG, message);
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

/**Perform Action Related to Permission*/
                }
                else {

                    String message = "Permission Denied: Cannot Read Phone State";
                    Log.i(TAG, message);
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

/**Disable Task Related to Permission*/
                }
        }
    }
}
