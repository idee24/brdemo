package com.example.android.brdemo.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class SecondReceiver extends BroadcastReceiver {

    private static final String TAG = SecondReceiver.class.getSimpleName();
    private String message = "Message from Broadcast Receiver 3";

    @Override
    public void onReceive(Context context, Intent intent) {

            Log.i(TAG, message);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }
}
