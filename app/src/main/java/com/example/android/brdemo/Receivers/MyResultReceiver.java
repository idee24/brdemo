package com.example.android.brdemo.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.android.brdemo.MainActivity;

public class MyResultReceiver extends BroadcastReceiver {
    final String TAG = MainActivity.InnerReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {


        if (isOrderedBroadcast()) {
            int initialCode = getResultCode();
            String data = getResultData();
            Bundle broadcastBundle = getResultExtras(true);
            String bundleData = broadcastBundle.getString("title");

            Log.i(TAG, "Initial Code: " + initialCode + " Data: " + data + "BundleData: " + bundleData);

            String message = "Message from MyResult BroadCast Receiver 2";

            Log.i(TAG, message);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

            String newBundleString = "Bundle Data: " + bundleData + "Result Receiver";
            broadcastBundle.putString("title", newBundleString);

            setResult(20, bundleData + "Result Receiver", broadcastBundle);
        }
        }
}
