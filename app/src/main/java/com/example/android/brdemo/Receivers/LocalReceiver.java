package com.example.android.brdemo.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.example.android.brdemo.LocalBroadcast;

public class LocalReceiver extends BroadcastReceiver {

    public static final String TAG = LocalReceiver.class.getSimpleName();
    public final String message = "Local Broadcast Received";
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i(TAG, message);
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

        int value1 = intent.getIntExtra("value1", 0);
        int value2 = intent.getIntExtra("value2", 0);
        int value3 = value1 + value2;

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
        Intent localReceiverIntent = new Intent("my.result.intent");
        localReceiverIntent.putExtra("value3", value3);

        localBroadcastManager.sendBroadcast(localReceiverIntent);
    }
}
