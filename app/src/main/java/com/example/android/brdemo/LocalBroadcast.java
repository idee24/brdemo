package com.example.android.brdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.android.brdemo.Receivers.LocalReceiver;
import com.example.android.brdemo.Receivers.Receiver;

public class LocalBroadcast extends AppCompatActivity {

    private LocalBroadcastManager localBroadcastManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_broadcast);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    public void sendLocalBroadcast(View view) {

        Intent localBroadcastIntent = new Intent(LocalBroadcast.this, LocalReceiver.class);
        localBroadcastIntent.putExtra("value1", 10);
        localBroadcastIntent.putExtra("value2", 20);
        sendBroadcast(localBroadcastIntent);
    }

    private BroadcastReceiver resultReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int value3 = intent.getIntExtra("value3", 0);
            Toast.makeText(LocalBroadcast.this, "Sum is: " + value3, Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter resultIntentFilter = new IntentFilter();
        resultIntentFilter.addAction("my.result.intent");
        localBroadcastManager.registerReceiver(resultReceiver, resultIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        localBroadcastManager.unregisterReceiver(resultReceiver);
    }
}
