package com.example.android.brdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class StickyBroadcast extends AppCompatActivity {

    private static final String TAG = StickyBroadcast.class.getSimpleName();
    private String full = "Battery Full";
    private String charging = "Battery Charging";
    private String discharging = "Battery Discharging";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sticky_broadcast);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(batteryStatusReceiver);
    }

    public void methodTwo(View view) {

        IntentFilter batteryStatusIntentFilter = new IntentFilter();
        batteryStatusIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);

        registerReceiver(batteryStatusReceiver, batteryStatusIntentFilter);

    }

    private BroadcastReceiver batteryStatusReceiver = new BroadcastReceiver() {

        private static final String TAG = "Charging State Receiver";
        @Override
        public void onReceive(Context context, Intent intent) {

            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

            String message = "Sticky Intent Status: ";
            Log.i(TAG, message + status);

            displayBatteryStatus(status);
        }
    };

    public void methodOne(View view) {


        IntentFilter methodOneIntentFilter = new IntentFilter();
        methodOneIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        
        Intent stickyIntent = registerReceiver(null, methodOneIntentFilter);

        int status = stickyIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        String message = "Sticky Intent Status: ";
        Log.i(TAG, message + status);
        displayBatteryStatus(status);

    }

    private void displayBatteryStatus(int status){

        if (status == BatteryManager.BATTERY_STATUS_CHARGING){

            Toast.makeText(this, charging, Toast.LENGTH_SHORT).show();
        }

        if (status == BatteryManager.BATTERY_STATUS_DISCHARGING){

            Toast.makeText(this, discharging, Toast.LENGTH_SHORT).show();
        }

        if (status == BatteryManager.BATTERY_STATUS_FULL){

            Toast.makeText(this, full, Toast.LENGTH_SHORT).show();
        }
    }

}
